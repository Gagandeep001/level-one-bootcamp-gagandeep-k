//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
int inputx()
{
    float x;
    scanf("%f",&x);
    return x;
}
int inputy()
{
    float y;
    scanf("%f",&y);
    return y;
}
void print(float dist)
{
    printf("distance=%f\n",dist);
}
float calc(float x1, float x2,float y1,float y2)
{
    return(sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1))));
}
int main()
{
    float x1,y1,x2,y2;
    printf("input x1:");
    x1=inputx();
    printf("input y1:");
    y1=inputy();
    printf("input x2:");
    x2=inputx();
    printf("input y2:");
    y2=inputy();
    float dist=calc(x1,y1,x2,y2);
    print(dist);
    return 0;
}
